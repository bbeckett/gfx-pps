/*
 * Copyright (c) 2019-2020 Collabora, Ltd.
 * Author: Aguado Puig, Quim <quim.aguado@collabora.com>
 * Author: Antonio Caggiano <antonio.caggiano@collabora.com>
 *
 * SPDX-License-Identifier: MIT
 */

#pragma once

#include <pps/pds.h>
#include <weston-debug-client-protocol.h>

#include "pps/weston/parser.h"
#include "pps/weston/track.h"

namespace pps::weston
{


/// @return A buffer of bytes read from a file descriptor
TimelineBuffer read_buffer( int fd );


class TimelineDataSource final: public perfetto::DataSource<TimelineDataSource>
{
  public:
	~TimelineDataSource() override { reset(); }

	static const char* get_name() { return "weston.debug.timeline"; }
	static const char* get_stream_name() { return "timeline"; }

	/// @brief File path to read to parse json weston timeline.
	/// If it is null (default), the data source connects to weston
	static const char* read_file;

	/// @return The current state of this data-source
	static State get_state() { return state; }

	/// @brief Perfetto trace callback
	static void trace_callback( TraceContext ctx );

	/// Perfetto callbacks
	void OnSetup( const SetupArgs& args ) override;
	void OnStart( const StartArgs& args ) override;
	void OnStop( const StopArgs& args ) override;

	/// @return The weston parser used by this data source
	const Parser& get_parser() const { return parser; }

	/// @brief Reads bytes from a file descriptor until EOF
	/// to parse weston objects and timepoints
	/// @param read_fd File descriptor to read from
	std::vector<WestonTimepoint> read_timepoints( int read_fd );

  private:
	/// @brief To be called on reset state
	void reset();

	void sample();

	/// Weston debug callbacks
	static void global_add( void* data, wl_registry* registry, uint32_t id, const char* interface, uint32_t version );
	static void global_remove( void *data, wl_registry* registry, uint32_t id );
	static void debug_available( void* data, weston_debug_v1* debug, const char* name, const char* description );
	static void handle_stream_failure( void* data, weston_debug_stream_v1* stream, const char* msg );
	static void handle_stream_complete( void* data, weston_debug_stream_v1* stream );

	/// @brief Perfetto callback helper
	/// If not already sent, it sends a new track descriptor
	void send_track_descriptor( const Track& track, const Track* parent = nullptr );

	/// @brief This perfetto callback helper sends a new track event for a timepoint
	/// associating it to the thread track of the surface, or output, it refers to
	void send_track_event(
		const WestonTimepoint& timepoint,
		perfetto::protos::pbzero::TrackEvent::Type type );

	/// @brief Send a trace packet for a weston timepoint
	/// @param ctx Trace context used to create the packet
	/// @param event Weston timepoint event to trace
	void send_packet( WestonTimepoint& event );

	static State state;

	// Keep track of current context to avoid passing it around in helper functions
	TraceContext* ctx = nullptr;

	wl_display* display = nullptr;
	wl_registry* registry = nullptr;

	const weston_debug_v1_listener debug_listener = { debug_available };

	const wl_registry_listener registry_listener = {
		.global = global_add,
		.global_remove = global_remove,
	};

	const weston_debug_stream_v1_listener stream_listener = {
		handle_stream_complete,
		handle_stream_failure,
	};

	bool stream_complete = false;

	weston_debug_stream_v1* stream = nullptr;

	/// Read and write ends of a pipe
	int read_fd = -1;
	int write_fd = -1;

	/// Used to store butes read from the pipe
	TimelineBuffer json_buffer = TimelineBuffer();

	/// Used to parse a json string into weston timeline events
	Parser parser = Parser();

	/// Perfetto parent tracks
	std::vector<TrackId> parent_tracks;

	/// Perfetto tracks
	std::vector<TrackId> tracks;

	weston_debug_v1* debug_iface = nullptr;
	bool stream_exists = false;
};


}  // namespace pps::weston
