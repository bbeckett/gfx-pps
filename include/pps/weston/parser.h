/*
 * Copyright (c) 2020 Collabora, Ltd.
 * Author: Antonio Caggiano <antonio.caggiano@collabora.com>
 *
 * SPDX-License-Identifier: MIT
 */

#pragma once

#include <array>
#include <vector>
#include <string>
#include <optional>
#include <rapidjson/document.h>

#include "weston_ds.h"


namespace pps::weston
{


using TimelineBuffer = std::vector<char>;


class Parser
{
  public:
	/// @brief Parses a buffer containing JSON weston timeline events
	/// @return The list of event parsed from the timeline and the number of chars parsed correcly
	std::pair<std::vector<WestonTimepoint>, size_t> parse( const TimelineBuffer& buffer );

	/// @return The list of objects parsed so far
	const std::vector<WestonObject>& get_objects() const { return objects; }

	/// @return The weston object with that ID; nullptr if not found
	WestonObject* get_object( WestonId id );

	/// @return The number of errors accumulated during all parsing
	size_t get_error_count() const { return error_count; }

  private:
	/// @brief Adds a new weston object, or updates an old one if already present
	void add_or_update( WestonObject&& obj );

	/// @brief Parses a json respresentation of a @ref WestonTimepoint
	/// @return Some newly created weston timepoints
	std::vector<WestonTimepoint> parse_timepoint( const rapidjson::Document& doc );

	/// Keeps track of the number of errors
	size_t error_count = 0;

	/// Store weston objects as they are parsed
	std::vector<WestonObject> objects;
};


} // namespace pps::weston
