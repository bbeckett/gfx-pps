#pragma once

#include <string>
#include <pps/perfetto/perfetto.h>

namespace pps::weston
{

class WestonObject;
class WestonTimepoint;
class TimelineDataSource;

using TrackId = uint64_t;


/// @brief Helper class which defines a Perfetto track
struct Track
{
	TrackId id = 0;
	std::string name = "Undefined";

	void send(
		const WestonTimepoint& event,
		perfetto::protos::pbzero::TrackEvent::Type type,
		protozero::MessageHandle<perfetto::protos::pbzero::TracePacket> packet );
};


/// @param output Tracks are grouped by output
/// @param surface Is surface is null the returned track is associated only with the output
/// @return A track for the pair output/surface
Track get_track( const WestonObject& output, const WestonObject* surface = nullptr );


/// @param output Output used for grouping child tracks
/// @return A parent track which groups tracks related to the same output
Track get_parent_track( const WestonObject& output );


/// @param output Output referred by the returned track
/// @return A track for vblank slices
Track get_vblank_track( const WestonObject& output );


/// @param output Output referred by the returned track
/// @return A track for repaint required and repaint delay slices
Track get_repaint_req_track( const WestonObject& output );


} // namespace pps::weston
