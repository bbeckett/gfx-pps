/*
 * Copyright (c) 2020 Collabora, Ltd.
 * Author: Antonio Caggiano <antonio.caggiano@collabora.com>
 *
 * SPDX-License-Identifier: MIT
 */

#pragma once

#include <cstdint>
#include <cassert>
#include <vector>
#include <functional>
#include <variant>


namespace pps::gpu
{


enum class CounterBlock
{
	UNDEFINED = -1,

	JOB_MANAGER = 0,
	TILER = 1,
	L2_MMU = 2,
	SHADER_CORE = 3,

	MAX_VALUE
};


/// @return A string representation for the counter block
constexpr const char* to_string( CounterBlock block )
{
	switch ( block )
	{
	case CounterBlock::JOB_MANAGER: return "JOB_MANAGER";
	case CounterBlock::TILER: return "TILER";
	case CounterBlock::L2_MMU: return "L2_MMU";
	case CounterBlock::SHADER_CORE: return "SHADER_CORE";
	default:
		assert( false && "Invalid counter" );
		return "INVALID_BLOCK";
	}
}


class Device;

class Counter
{
  public:
	/// @brief A counter value can be of different types depending on what it represents:
	/// cycles, cycles-per-instruction, percentages, bytes, and so on.
	using Value = std::variant<int64_t, double>;

	/// @param c Counter which we want to retrieve a value
	/// @param d Device used to sample performance counters
	/// @return The value of the counter
	using Getter = Value( const Counter& c, const Device& d );

	/// @brief The default getter is used by non-derived counters to retrieve
	/// their values from a device's performance counter memory dump
	static Value default_getter( const Counter& c, const Device& d );

	Counter() = default;

	/// @param name Name of the counter
	/// @param block Block this counter belongs to
	Counter( const char* name, CounterBlock block );

	/// @param name Name of the counter
	/// @param offset Offset of this counter within the others
	Counter( const char* name, int32_t offset );

	bool operator==( const Counter& c ) const;

	/// @return An id for the counter
	uint32_t get_id( const Device& device ) const;

	/// @param get New getter function for this counter
	void set_getter( const std::function<Getter>& get ) { getter = get; }

	/// @return Whether the counter is derived or not
	bool is_derived() const;

	/// @brief d Device used to sample performance counters
	/// @return Last sampled value for this counter
	Value get_value( const Device& d ) const { return getter( *this, d ); }

	/// Name of the counter
	const char* name;

	/// Offset of this counter within GPU counters list
	/// For derived counters it is negative and remains unused
	int32_t offset = -1;

	/// Block this counter belongs to
	CounterBlock block = CounterBlock::UNDEFINED;

  private:
	/// Returns the value of this counter within counters memory
	/// Derived counters must use getter different than default
	std::function<Getter> getter = default_getter;
};


/// @return The underlying u32 value
template <typename T>
constexpr uint32_t to_u32( T&& elem )
{
	return static_cast<uint32_t>( elem );
}


} // namespace pps::gpu
