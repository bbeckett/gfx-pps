/*
 * Copyright (c) 2020 Collabora, Ltd.
 * Author: Antonio Caggiano <antonio.caggiano@collabora.com>
 *
 * SPDX-License-Identifier: MIT
 */

#pragma once

#include "pps/gpu/device.h"
#include <drm/panfrost_drm.h>

namespace pps::gpu
{


/// @param A list of mali counter names
/// @return A list of available counters
std::vector<Counter> create_available_counters( const std::vector<const char*>& counter_names );


/// @return The number of performance counters
size_t query_counters_count( const uint32_t cores );


/// @brief Panfrost implementation of DRM device
class PanfrostDevice : public Device
{
  public:
	static constexpr uint32_t counters_per_block = 64;
	struct drm_panfrost_get_param gpu_id = {0,};

	/// @todo Some implementations use 8 bytes interfaces instead of 16
	static constexpr uint32_t l2_axi_width = 16;

	/// @todo RK3399 has a Mali T860 MP4 (quad-core)
	static constexpr uint32_t l2_axi_port_count = 4;

	PanfrostDevice();

	void enable_perfcnt() const override;
	void disable_perfcnt() const override;
	bool dump_perfcnt() override;
};


} // namespace pps::gpu
