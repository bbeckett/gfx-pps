/*
 * Copyright (c) 2020 Collabora, Ltd.
 * Author: Antonio Caggiano <antonio.caggiano@collabora.com>
 *
 * SPDX-License-Identifier: MIT
 */

#pragma once

#include <string>
#include <vector>

#include "pps/gpu/panfrost/counters.h"

namespace pps::gpu
{


/// @brief Helper class for a DRM device
class Device
{
  public:
	Device() = default;
	Device( const char* drm_version_name );

	Device( Device&& o );
	Device& operator=( Device&& o );

	Device( const Device& o ) = delete;
	Device& operator=( const Device& o ) = delete;

	virtual ~Device();

	/// @return Whether a device has a valid name
	operator bool() const { return !name.empty(); }

	bool is_open() const { return fd >= 0; }

	void open();
	void close();

	virtual void enable_perfcnt() const = 0;
	virtual void disable_perfcnt() const = 0;

	/// @brief Samples performance counters which means
	/// asking the GPU to dump them to a user space buffer
	/// @return Whether it was able to dump performance counters
	virtual bool dump_perfcnt() = 0;

	uint32_t get_tile_size() const { return tile_size; }

	const std::vector<uint32_t>& get_counters() const { return counters; }

	/// File descriptor of the DRM device
	int fd = -1;

	/// DRM device version name
	std::string name;

	/// Number of cores
	uint32_t cores = 0;

	/// List of counters exposed by the GPU
	std::vector<Counter> available_counters;

	/// Memory where to dump performance counters
	std::vector<uint32_t> counters;

	uint32_t tile_size = 0;
};


} // namespace pps::gpu
