/*
 * Copyright (c) 2020 Collabora, Ltd.
 * Author: Antonio Caggiano <antonio.caggiano@collabora.com>
 *
 * SPDX-License-Identifier: MIT
 */

#include "pps/weston/parser.h"

#include <pps/perfetto/perfetto.h>

#include "pps/algorithm.h"

namespace pps::weston
{


/// @return A timestamp (ns) representation of the timespec
uint64_t to_timestamp( timespec ts )
{
	return ts.tv_sec * 1000000000 + ts.tv_nsec;
}


timespec min( timespec a, timespec b )
{
	return to_timestamp( a ) < to_timestamp( b ) ? a : b;
}


timespec max( timespec a, timespec b )
{
	return to_timestamp( a ) > to_timestamp( b ) ? a : b;
}


std::string to_string( const WestonObject& wo )
{
	if ( !wo.name.empty() )
	{
		return wo.name;
	}
	else
	{
		return wo.description;
	}
}


/// @return Conversion of a string to a weston object type
WestonObject::Type object_from_string( const std::string_view& name )
{
	if ( name == "weston_surface" )
	{
		return WestonObject::Type::SURFACE;
	}
	else if ( name == "weston_output" )
	{
		return WestonObject::Type::OUTPUT;
	}

	PERFETTO_ELOG( "Unknown weston object type: %s", name.data() );
	return WestonObject::Type::UNDEFINED;
}


/// @return Conversion of a string to a weston timepoint type
WestonTimepoint::Type timepoint_from_string( const std::string_view& name )
{
	if ( name == "core_commit_damage" )
	{
		return WestonTimepoint::Type::CORE_COMMIT_DAMAGE;
	}
	else if ( name == "core_flush_damage" )
	{
		return WestonTimepoint::Type::CORE_FLUSH_DAMAGE;
	}
	else if ( name == "core_repaint_begin" )
	{
		return WestonTimepoint::Type::CORE_REPAINT_BEGIN;
	}
	else if ( name == "core_repaint_finished" )
	{
		return WestonTimepoint::Type::CORE_KMS_FLIP_END;
	}
	else if ( name == "core_repaint_posted" )
	{
		return WestonTimepoint::Type::CORE_KMS_FLIP_BEGIN;
	}
	else if ( name == "core_repaint_enter_loop" )
	{
		return WestonTimepoint::Type::CORE_REPAINT_ENTER_LOOP;
	}
	else if ( name == "core_repaint_exit_loop" )
	{
		return WestonTimepoint::Type::CORE_REPAINT_EXIT_LOOP;
	}
	else if ( name == "core_repaint_req" )
	{
		return WestonTimepoint::Type::CORE_REPAINT_REQ_BEGIN;
	}
	else if ( name == "renderer_gpu_begin" )
	{
		return WestonTimepoint::Type::RENDERER_GPU_BEGIN;
	}
	else if ( name == "renderer_gpu_end" )
	{
		return WestonTimepoint::Type::RENDERER_GPU_END;
	}

	PERFETTO_ELOG( "Unknown weston timepoint type: %s", name.data() );
	return WestonTimepoint::Type::UNDEFINED;
}


/// @return The name of a weston timepoint to use for Perfetto TrackEvents
std::string name_from_timepoint( const WestonTimepoint::Type type )
{
	switch ( type )
	{
		case WestonTimepoint::Type::CORE_COMMIT_DAMAGE:
		case WestonTimepoint::Type::CORE_FLUSH_DAMAGE:
			return "Pending damage";
		case WestonTimepoint::Type::CORE_REPAINT_BEGIN:
		case WestonTimepoint::Type::CORE_REPAINT_END:
			return "Repaint CPU";
		case WestonTimepoint::Type::CORE_KMS_FLIP_BEGIN:
		case WestonTimepoint::Type::CORE_KMS_FLIP_END:
			return "KMS flip";
		case WestonTimepoint::Type::CORE_REPAINT_ENTER_LOOP:
		case WestonTimepoint::Type::CORE_REPAINT_EXIT_LOOP:
			return "Repaint loop";
		case WestonTimepoint::Type::CORE_REPAINT_REQ_BEGIN:
		case WestonTimepoint::Type::CORE_REPAINT_REQ_END:
			return "Repaint required";
		case WestonTimepoint::Type::CORE_REPAINT_DELAY_BEGIN:
		case WestonTimepoint::Type::CORE_REPAINT_DELAY_END:
			return "Repaint delay";
		case WestonTimepoint::Type::RENDERER_GPU_BEGIN:
		case WestonTimepoint::Type::RENDERER_GPU_END:
			return "Renderer gpu";
		case WestonTimepoint::Type::VBLANK_BEGIN:
		case WestonTimepoint::Type::VBLANK_END:
			return "Vblank";
		default:
			PERFETTO_ELOG( "Unknown name weston timepoint type %d", static_cast<int>( type ) );
			return "unknown";
	}
}


std::string get_string( const rapidjson::Document& doc, const char* name )
{
	if ( doc.HasMember( name ) && doc[name].IsString() )
	{
		return doc[name].GetString();
	}

	return {};
}


/// @brief Parses a json respresentation of a WestonObject
/// @return A newly created weston object
WestonObject parse_object( const rapidjson::Document& doc )
{
	WestonObject ret;

	assert( doc.HasMember( "id" ) && "Weston object should have an id" );
	ret.id = doc["id"].GetUint();

	auto type_str = get_string( doc, "type" );
	ret.type = object_from_string( type_str );
	ret.name = get_string( doc, "name" );
	ret.description = get_string( doc, "desc" );

	return ret;
}


std::vector<WestonTimepoint> Parser::parse_timepoint( const rapidjson::Document& doc )
{
	std::vector<WestonTimepoint> timepoints;

	WestonTimepoint timepoint;

	assert( doc.HasMember( "T" ) && "Weston timepoint should have a T" );
	auto& tvalue = doc["T"];
	assert( tvalue.IsArray() && "T should be an array" );
	assert( tvalue.Size() == 2 && "T size should be 2" );
	timepoint.ts.tv_sec = tvalue[0].GetInt64();
	timepoint.ts.tv_nsec = tvalue[1].GetInt64();

	timepoint.type = timepoint_from_string( get_string( doc, "N" ) );

	timepoint.name = name_from_timepoint( timepoint.type );

	// Check if it refers to a surface
	if ( doc.HasMember( "ws" ) )
	{
		auto& object_id = doc["ws"];
		assert( object_id.IsUint() && "Weston surface ID should be uint" );
		timepoint.surface = object_id.GetUint();
	}

	// Check if it refers to an output
	if ( doc.HasMember( "wo" ) )
	{
		auto& object_id = doc["wo"];
		assert( object_id.IsUint() && "Weston surface ID should be uint" );
		timepoint.output = object_id.GetUint();
	}

	auto output = get_object( timepoint.output );

	// Before beginning a new Repaint CPU slice
	if ( timepoint.type == WestonTimepoint::Type::CORE_REPAINT_BEGIN )
	{
		// Check if we need to end a repaint_required
		if ( output->repaint_required )
		{
			WestonTimepoint req_end;
			req_end.type = WestonTimepoint::Type::CORE_REPAINT_REQ_END;
			req_end.ts = timepoint.ts;
			req_end.name = name_from_timepoint( req_end.type );
			req_end.output = timepoint.output;
			timepoints.emplace_back( std::move( req_end ) );
			output->repaint_required = false;
		}
	
		// Check if we need to end a repaint_delay
		if ( output->repaint_delay )
		{
			WestonTimepoint delay_end;
			delay_end.type = WestonTimepoint::Type::CORE_REPAINT_DELAY_END;
			delay_end.ts = timepoint.ts;
			delay_end.name = name_from_timepoint( delay_end.type );
			delay_end.output = timepoint.output;
			timepoints.emplace_back( std::move( delay_end ) );
			output->repaint_delay = false;
		}
	}

	// Before beginning a new KMS flip slice we end a Repaint CPU slice
	if ( timepoint.type == WestonTimepoint::Type::CORE_KMS_FLIP_BEGIN )
	{
		WestonTimepoint repaint_end;
		repaint_end.type = WestonTimepoint::Type::CORE_REPAINT_END;
		repaint_end.ts = timepoint.ts;
		repaint_end.name = name_from_timepoint( repaint_end.type );
		repaint_end.output = timepoint.output;
		timepoints.emplace_back( std::move( repaint_end ) );
	}

	if ( timepoint.type == WestonTimepoint::Type::CORE_REPAINT_REQ_BEGIN )
	{
		if ( output->repaint_required )
		{
			return {}; // Skip reemitting this event
		}
		output->repaint_required = true;
	}

	timepoints.emplace_back( timepoint );

	if ( timepoint.type == WestonTimepoint::Type::CORE_KMS_FLIP_END )
	{
		// Core repaint finished might have a vblank
		if ( doc.HasMember( "vblank" ) )
		{
			auto& vblank_value = doc["vblank"];
			assert( vblank_value.IsArray() && "Vblank should be an array" );
			assert( vblank_value.Size() == 2 && "Vblank size should be 2" );
			timespec vblank_ts = {};
			vblank_ts.tv_sec = vblank_value[0].GetInt64();
			vblank_ts.tv_nsec = vblank_value[1].GetInt64();

			uint64_t event_ns = to_timestamp( timepoint.ts );
			uint64_t vblank_ns = to_timestamp( vblank_ts );

			WestonTimepoint vblank_begin;
			vblank_begin.type = WestonTimepoint::Type::VBLANK_BEGIN;
			vblank_begin.ts = vblank_ts;
			vblank_begin.name = name_from_timepoint( vblank_begin.type );
			vblank_begin.output = output->id;
			timepoints.emplace_back( std::move( vblank_begin ) );

			WestonTimepoint vblank_end;
			vblank_end.type = WestonTimepoint::Type::VBLANK_END;
			vblank_ts.tv_nsec += 10000;
			vblank_end.ts = vblank_ts;
			vblank_end.name = name_from_timepoint( vblank_end.type );
			vblank_end.output = output->id;
			timepoints.emplace_back( std::move( vblank_end ) );
		}

		if ( output->repaint_required )
		{
			// End and consume a repaint_required
			WestonTimepoint req_end;
			req_end.type = WestonTimepoint::Type::CORE_REPAINT_REQ_END;
			req_end.ts = timepoint.ts;
			req_end.name = name_from_timepoint( req_end.type );
			req_end.output = output->id;
			timepoints.emplace_back( std::move( req_end ) );
			output->repaint_required = false;

			// Start a repaint_delay
			WestonTimepoint delay_begin;
			delay_begin.type = WestonTimepoint::Type::CORE_REPAINT_DELAY_BEGIN;
			delay_begin.ts = timepoint.ts;
			delay_begin.name = name_from_timepoint( delay_begin.type );
			delay_begin.output = output->id;
			timepoints.emplace_back( std::move( delay_begin ) );
			output->repaint_delay = true;
		}
	}

	return timepoints;
}


WestonObject* Parser::get_object( const WestonId id )
{
	if ( auto it = FIND_IF( objects, [id]( auto& obj ) { return obj.id == id; } );
		it != std::end( objects ) )
	{
		return &*it;
	}

	return nullptr;
}


void Parser::add_or_update( WestonObject&& obj )
{
	assert( obj.id && "Id 0 is not valid for weston objects" );

	if ( auto old_obj = FIND( objects, obj );
		old_obj != std::end( objects ) )
	{
		// Update
		*old_obj = std::move( obj );
	}
	else // Add
	{
		objects.emplace_back( std::move( obj ) );
	}
}


std::pair<std::vector<WestonTimepoint>, size_t> Parser::parse( const TimelineBuffer& buffer )
{
	std::vector<WestonTimepoint> ret;
	size_t chars_parsed = 0;

	if ( buffer.empty() )
	{
		return { ret, chars_parsed };
	}

	using namespace rapidjson;

	auto jstream = StringStream( buffer.data() );
	Document doc;

	while ( true )
	{
		doc.ParseStream<kParseStopWhenDoneFlag>( jstream );

		if ( doc.HasParseError() )
		{
			if ( doc.GetParseError() == ParseErrorCode::kParseErrorDocumentEmpty )
			{
				// Stream empty
				break;
			}

			++error_count;
			break;
		}

		if ( doc.HasMember( "id" ) )
		{
			add_or_update( parse_object( doc ) );
		}
		else if ( doc.HasMember( "T" ) )
		{
			auto timepoints = parse_timepoint( doc );
			// Append vector
			ret.insert( std::end( ret ), std::begin( timepoints ), std::end( timepoints ) );
		}
		else
		{
			PERFETTO_ELOG( "Unknown timeline object" );
		}

		chars_parsed = jstream.Tell();
	}

	return { ret, chars_parsed };
}


} // namespace pps::weston
