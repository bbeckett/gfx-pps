/*
 * Copyright (c) 2020 Collabora, Ltd.
 * Author: Antonio Caggiano <antonio.caggiano@collabora.com>
 *
 * SPDX-License-Identifier: MIT
 */

#include "pps/weston/track.h"

#include <cassert>

#include "pps/weston/weston_ds.h"
#include "pps/weston/timeline_ds.h"


namespace pps::weston
{


Track get_track( const WestonObject& output, const WestonObject* surface )
{
	Track parent = get_parent_track( output );

	// If it has a surface, the event refers to that surface.
	// With no surface, the event goes to the output track.
	const WestonObject* weston_object = surface ? surface : &output;
	assert( weston_object && "Event refers to invalid objects" );

	// Combine this track id with its parent
	auto track_id = parent.id + weston_object->id;

	return Track {
		track_id,
		to_string( *weston_object )
	};
}


Track get_parent_track( const WestonObject& output )
{
	assert( output.id && "Invalid id for parent track" );
	return Track {
		1000 * output.id,
		to_string( output )
	};
}


Track get_repaint_req_track( const WestonObject& output )
{
	assert( output.id && "Invalid id for repaint-req track" );
	return Track {
		get_parent_track( output ).id + 999,
		"Repaint required - " + to_string( output )
	};
}


Track get_vblank_track( const WestonObject& output )
{
	assert( output.id && "Invalid id for repaint-req track" );
	return Track {
		get_parent_track( output ).id + 998,
		"Vblank - " + to_string( output )
	};
}


void Track::send(
	const WestonTimepoint& event,
	perfetto::protos::pbzero::TrackEvent::Type type,
	protozero::MessageHandle<perfetto::protos::pbzero::TracePacket> packet )
{
	packet->set_timestamp( event.get_timestamp() );

	auto track_event = packet->set_track_event();
	track_event->set_name( event.name );
	track_event->set_type( type );
	track_event->set_track_uuid( id );
	track_event->add_categories( "weston" );
}


} // namespace pps::weston
