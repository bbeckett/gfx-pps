/*
 * Copyright (c) 2020 Collabora, Ltd.
 * Author: Antonio Caggiano <antonio.caggiano@collabora.com>
 *
 * SPDX-License-Identifier: MIT
 */

#include <cstdlib>
#include <thread>
#include <sys/types.h>
#include <sys/stat.h>
#include <unistd.h>

#include <pps/weston/timeline_ds.h>


int main( int argc, const char** argv )
{
	using namespace pps;

	// Default behaviour is data source connects to weston as a client
	// and parses the json weston timeline through its debug protocol.
	// When a command line argument is passed, the data source will
	// try to open it to parse its content as json weston timeline.
	if ( argc == 2 )
	{
		struct stat stat_buf;
		if ( stat( argv[1], &stat_buf ) < 0 )
		{
			PERFETTO_FATAL( "Failed checking file" );
		}
		weston::TimelineDataSource::read_file = argv[1];
	}

	// Connects to the system tracing service
	perfetto::TracingInitArgs args;
	args.backends = perfetto::kSystemBackend;
	perfetto::Tracing::Initialize( args );

	perfetto::DataSourceDescriptor dsd;
	dsd.set_name( weston::TimelineDataSource::get_name() );
	weston::TimelineDataSource::Register( dsd );

	while ( true )
	{
		switch ( weston::TimelineDataSource::get_state() )
		{
			using namespace std::chrono_literals;

			case State::Stop:
			{
				// Just wait until it starts
				std::this_thread::sleep_for( 2ms );
				break;
			}
			case State::Start:
			{
				weston::TimelineDataSource::Trace( weston::TimelineDataSource::trace_callback );
				/// @todo Can this time be specified by a perfetto config file?
				std::this_thread::sleep_for( 2ms );
				break;
			}
			default:
			{
				assert( false && "Invalid DataSource state" );
			}
		}
	}

	return EXIT_SUCCESS;
}
