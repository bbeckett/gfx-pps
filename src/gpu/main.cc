/*
 * Copyright (c) 2019-2020 Collabora, Ltd.
 * Author: Antonio Caggiano <antonio.caggiano@collabora.com>
 *
 * SPDX-License-Identifier: MIT
 */

#include <cstdlib>

#include "panfrost/gpu_ds.h"


int main( int argc, const char** argv )
{
	using namespace pps;

	// Connects to the system tracing service
	perfetto::TracingInitArgs args;
	args.backends = perfetto::kSystemBackend;
	perfetto::Tracing::Initialize( args );

	perfetto::DataSourceDescriptor dsd;
	dsd.set_name( gpu::PanfrostDataSource::name );
	gpu::PanfrostDataSource::Register( dsd );

	gpu::PanfrostDataSource::drm_device_name = "panfrost";

	while ( true )
	{
		gpu::PanfrostDataSource::Trace( gpu::PanfrostDataSource::trace_callback );
	}

	return EXIT_SUCCESS;
}
