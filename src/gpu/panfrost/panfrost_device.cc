/*
 * Copyright (c) 2019-2020 Collabora, Ltd.
 * Author: Antonio Caggiano <antonio.caggiano@collabora.com>
 * Author: Rohan Garg <rohan.garg@collabora.com>
 *
 * SPDX-License-Identifier: MIT
 */

#include "pps/gpu/panfrost_device.h"

#include <cstring>
#include <xf86drm.h>
#include <pps/perfetto/perfetto.h>
#include <pps/pds.h>
#include <pps/algorithm.h>

#include "pps/gpu/panfrost/hwc_names.h"

namespace pps::gpu
{


/// @param gpu_id drm_panfrost_get_param struct that contains the GPU ID
/// @return A list of counter names for the drm device
std::vector<const char*> create_counter_names(const struct drm_panfrost_get_param* gpu_id)
{
	const char* const* base = nullptr;
	size_t size = 0;

	switch ( gpu_id->value )
	{
		#define SET_BASE_SIZE( array, base, size ) { base = array; size = sizeof( array ) / sizeof( array[0] ); }
	case 0x600:
		SET_BASE_SIZE( mali_userspace::hardware_counters_mali_t60x, base, size ); break;
	case 0x620:
		SET_BASE_SIZE( mali_userspace::hardware_counters_mali_t62x, base, size ); break;
	case 0x720:
		SET_BASE_SIZE( mali_userspace::hardware_counters_mali_t72x, base, size ); break;
	case 0x750:
		SET_BASE_SIZE( mali_userspace::hardware_counters_mali_t76x, base, size ); break;
	case 0x820:
		SET_BASE_SIZE( mali_userspace::hardware_counters_mali_t82x, base, size ); break;
	case 0x830:
		SET_BASE_SIZE( mali_userspace::hardware_counters_mali_t83x, base, size ); break;
	case 0x860:
		SET_BASE_SIZE( mali_userspace::hardware_counters_mali_t86x, base, size ); break;
	case 0x880:
		SET_BASE_SIZE( mali_userspace::hardware_counters_mali_t88x, base, size ); break;
	default: PERFETTO_FATAL( "GPU ID not supported %llx", gpu_id->value );
	}

	return std::vector<const char*>( base, base + size );
}


/// @param name Name of the new counter
/// @param numerator Counter to use as numerator
/// @param denominator Counter to use as denominator
/// @return A derived counter which is the ratio of two counters
Counter create_ratio_counter( const char* name, const Counter& numerator, const Counter& denominator )
{
	Counter ret { name, numerator.block };
	ret.set_getter( [numerator, denominator]( const Counter& c, const Device& device ) -> Counter::Value {
		auto num_v = std::get<int64_t>( numerator.get_value( device ) );
		auto den_v = std::get<int64_t>( denominator.get_value( device ) );
		return ratio( num_v, den_v );
	} );
	return ret;
}


std::vector<Counter>::const_iterator find_by_name( const std::vector<Counter>& counters, const char* name )
{
	return FIND_IF( counters, [name]( const Counter& c ) { return std::strstr( c.name, name ); } );
}


bool contains( const std::vector<Counter>& counters, const std::vector<Counter>::const_iterator& iterator )
{
	return iterator != std::end( counters );
}


void add_ratio_counter( const char* name, const char* num_name, const char* den_name, std::vector<Counter>& counters )
{
	auto num = find_by_name( counters, num_name );
	auto den = find_by_name( counters, den_name );

	if ( contains( counters, num ) && contains( counters, den ) )
	{
		Counter ratio_counter = create_ratio_counter( name, *num, *den );
		counters.insert( std::next( num ), std::move( ratio_counter ) );
	}
}


void add_tripipe_counters( std::vector<Counter>& counters )
{
	auto tripipe_active_it = find_by_name( counters, "TRIPIPE_ACTIVE" );
	auto gpu_active_it = find_by_name( counters, "GPU_ACTIVE" );

	if ( contains( counters, tripipe_active_it ) && contains( counters, gpu_active_it ) ) {
		Counter tripipe_active = *tripipe_active_it;
		Counter gpu_active = *gpu_active_it;

		Counter tripipe_usage { "TRIPIPE_USAGE", tripipe_active.block };
		tripipe_usage.set_getter( [tripipe_active, gpu_active]( const Counter& c, const Device& device ) -> Counter::Value {
			auto num_v = std::get<int64_t>( tripipe_active.get_value( device ) );
			auto den_v = std::get<int64_t>( gpu_active.get_value( device ) );
			return ratio( num_v / double( device.cores ), den_v );
		} );

		counters.insert( std::next( tripipe_active_it ), std::move( tripipe_usage ) );
	}

	add_ratio_counter( "ARITH_USAGE", "ARITH_WORDS", "TRIPIPE_ACTIVE", counters );
}


void add_load_store_counters( std::vector<Counter>& counters )
{
	add_ratio_counter( "LS_USAGE", "LS_WORDS", "TRIPIPE_ACTIVE", counters );
	add_ratio_counter( "LS_MICRO_USAGE", "LS_ISSUES", "TRIPIPE_ACTIVE", counters );
	add_ratio_counter( "LS_CPI", "LS_WORDS", "LS_ISSUES", counters );
}


void add_load_store_cache_counters( std::vector<Counter>& counters )
{
	add_ratio_counter( "LSC_READ_HITRATE", "LSC_READ_HITS", "LSC_READ_OP", counters );
	add_ratio_counter( "LSC_WRITE_HITRATE", "LSC_WRITE_HITS", "LSC_WRITE_OP", counters );
	add_ratio_counter( "LSC_ATOMIC_HITRATE", "LSC_ATOMIC_HITS", "LSC_ATOMIC_OP", counters );
}


void add_texture_counters( std::vector<Counter>& counters )
{
	add_ratio_counter( "TEX_CPI", "TEX_WORDS", "TEX_ISSUES", counters );
}


void add_l2_counters( std::vector<Counter>& counters )
{
	add_ratio_counter( "L2_READ_HITRATE", "L2_READ_HIT", "L2_READ_LOOKUP", counters );
	add_ratio_counter( "L2_WRITE_HITRATE", "L2_WRITE_HIT", "L2_WRITE_LOOKUP", counters );
}


void add_l2_ext_read_counters( std::vector<Counter>& counters )
{
	auto read_beats_it = find_by_name( counters, "L2_EXT_READ_BEATS" );
	// Do not add anything if l2 ext read beats counter is not enabled
	if ( !contains( counters, read_beats_it ) )
		return;

	auto read_beats = *read_beats_it;

	// Add l2 ext read bytes counter
	auto read_bytes = Counter { "L2_EXT_READ_BYTES", read_beats.block };
	read_bytes.set_getter( [read_beats]( const Counter& c, const Device& device ) {
		auto beats = std::get<int64_t>( read_beats.get_value( device ) );
		auto panfrost_device = static_cast<const PanfrostDevice*>( &device );
		return beats * panfrost_device->l2_axi_width;
	} );

	counters.insert( std::next( read_beats_it ), std::move( read_bytes ) );

	auto gpu_active_it = find_by_name( counters, "GPU_ACTIVE" );
	// Do not add usage if gpu active counter is not enabled
	if ( !contains( counters, gpu_active_it ) )
		return;

	auto gpu_active = *gpu_active_it;

	// Add l2 ext read usage counter
	auto read_usage = Counter { "L2_EXT_READ_USAGE", read_beats.block };
	read_usage.set_getter( [read_beats, gpu_active]( const Counter& c, const Device& device ) {
		auto beats = std::get<int64_t>( read_beats.get_value( device ) );
		auto gpu = std::get<int64_t>( gpu_active.get_value( device ) );
		auto panfrost_device = static_cast<const PanfrostDevice*>( &device );
		return beats / double( gpu * panfrost_device->l2_axi_port_count );
	} );

	counters.insert( std::next( read_beats_it ), std::move( read_usage ) );
}


void add_l2_ext_write_counters( std::vector<Counter>& counters )
{
	auto write_beats_it = find_by_name( counters, "L2_EXT_WRITE_BEATS" );
	// Do not add anything if l2 ext write beats counter is not enabled
	if ( !contains( counters, write_beats_it ) )
		return;

	auto write_beats = *write_beats_it;

	// Add l2 ext write bytes counter
	auto write_bytes = Counter { "L2_EXT_WRITE_BYTES", write_beats.block };
	write_bytes.set_getter( [write_beats]( const Counter& c, const Device& device ) {
		auto beats = std::get<int64_t>( write_beats.get_value( device ) );
		auto panfrost_device = static_cast<const PanfrostDevice*>( &device );
		return beats * panfrost_device->l2_axi_width;
	} );

	counters.insert( std::next( write_beats_it ), std::move( write_bytes ) );

	auto gpu_active_it = find_by_name( counters, "GPU_ACTIVE" );
	// Do not add write usage if gpu active counter is not enabled
	if ( !contains( counters, gpu_active_it ) )
		return;

	auto gpu_active = *gpu_active_it;

	// Add l2 ext write usage counter
	auto write_usage = Counter { "L2_EXT_WRITE_USAGE", write_beats.block };
	write_usage.set_getter( [write_beats, gpu_active]( const Counter& c, const Device& device ) {
		auto beats = std::get<int64_t>( write_beats.get_value( device ) );
		auto gpu = std::get<int64_t>( gpu_active.get_value( device ) );
		auto panfrost_device = static_cast<const PanfrostDevice*>( &device );
		return beats / double( gpu * panfrost_device->l2_axi_port_count );
	} );

	counters.insert( std::next( write_beats_it ), std::move( write_usage ) );
}


void add_l2_ext_counters( std::vector<Counter>& counters )
{
	add_l2_ext_read_counters( counters );
	add_l2_ext_write_counters( counters );
}


void add_derived_counters( std::vector<Counter>& counters )
{
	if ( auto it = find_by_name( counters, "JS0_TASKS" );
		contains( counters, it ) ) {
		auto js0_tasks = *it;
		auto pixel_count = Counter { "PIXEL_COUNT", js0_tasks.block };
		pixel_count.set_getter( [js0_tasks]( const Counter& counter, const Device& device ) {
			auto tasks = std::get<int64_t>( js0_tasks.get_value( device ) );
			return tasks * device.get_tile_size();
		} );
		counters.insert( std::next( it ), std::move( pixel_count ) );
	}

	add_tripipe_counters( counters );
	add_load_store_counters( counters );
	add_load_store_cache_counters( counters );
	add_texture_counters( counters );
	add_l2_counters( counters );
	add_l2_ext_counters( counters );
}


std::vector<Counter> create_available_counters( const std::vector<const char*>& counter_names )
{
	std::vector<Counter> counters;

	// Map counters names to Counter struct
	for ( int32_t offset = 0; offset < counter_names.size(); ++offset ) {
		const char* name = counter_names[offset];

		// Skip empty counter names
		if ( std::strcmp( name, "" ) == 0 )
			continue;

		auto counter = Counter { name, offset };
		counters.emplace_back( counter );
	}

	add_derived_counters( counters );

	return counters;
}


/// @return The number of cores of the GPU
uint32_t query_core_count(const int card_fd)
{
	if (card_fd <= 0) {
		PERFETTO_FATAL("Invalid GPU file descriptor");
		return 0;
	}

	struct drm_panfrost_get_param get_param = {0,};
	get_param.param = DRM_PANFROST_PARAM_SHADER_PRESENT;
	auto ret = drmIoctl(card_fd, DRM_IOCTL_PANFROST_GET_PARAM, &get_param);

	if (!check(ret, "Could not query GPU shader cores"))
		return 0;

	auto present = get_param.value;

	uint32_t cores = 0;

	while (present) {
		++cores;
		present >>= 1;
	}

	return cores;
}


size_t query_counters_count( const uint32_t cores )
{
	if (cores <= 0) {
		PERFETTO_FATAL("Invalid number of cores");
		// Guess a default size
		return PanfrostDevice::counters_per_block * 20;
	}

	// There are also blocks for job manager, tiler, and L2 / MMU
	auto blocks = cores + 3;

	return PanfrostDevice::counters_per_block * blocks;
}


uint32_t query_tile_size( const struct drm_panfrost_get_param* gpu_id )
{
	switch (gpu_id->value)
	{
		case 0x600:
		case 0x620:
		case 0x720:
			return 16 * 16;

		case 0x750:
		case 0x800:
		default:
			return 32 * 32;
	}
}

struct drm_panfrost_get_param query_gpu_id(int card_fd)
{
       if ( card_fd <= 0 )
       {
               PERFETTO_FATAL( "Invalid GPU file descriptor" );
	       return {};
       }

       struct drm_panfrost_get_param gpu_id = {0,};
       gpu_id.param = DRM_PANFROST_PARAM_GPU_PROD_ID;
       auto ret = drmIoctl( card_fd, DRM_IOCTL_PANFROST_GET_PARAM, &gpu_id );

       if ( !check( ret, "Could not query GPU ID" ) )
       {
               return {};
       }

       return gpu_id;
}


PanfrostDevice::PanfrostDevice()
: Device( "panfrost" )
{
	if ( !is_open() )
	{
		return;
	}

	gpu_id = query_gpu_id( fd );
	cores = query_core_count( fd );
	available_counters = create_available_counters( create_counter_names( &gpu_id ) );
	size_t counters_count = query_counters_count( cores );
	counters.resize( counters_count );
	tile_size = query_tile_size( &gpu_id );
	close();
}


void PanfrostDevice::enable_perfcnt() const
{
	drm_panfrost_perfcnt_enable perfcnt = {};
	perfcnt.enable = 1;
	perfcnt.counterset = 0;

	auto res = drmIoctl( fd, DRM_IOCTL_PANFROST_PERFCNT_ENABLE, &perfcnt );
	if ( !check( res, "Cannot enable performance counters" ) )
	{
		if ( res == -ENOSYS )
		{
			PERFETTO_FATAL( "Please enable unstable ioctls with: modprobe panfrost unstable_ioctls=1" );
		}
		PERFETTO_FATAL( "Please verify graphics card" );
	}
}


bool PanfrostDevice::dump_perfcnt()
{
	// Dump performance counters to buffer
	drm_panfrost_perfcnt_dump dump = {};
	dump.buf_ptr = reinterpret_cast<uintptr_t>( counters.data() );

	auto res = drmIoctl( fd, DRM_IOCTL_PANFROST_PERFCNT_DUMP, &dump );
	if ( !check( res, "Cannot dump" ) )
	{
		PERFETTO_ELOG( "Skipping sample" );
		return false;
	}

	return true;
}


void PanfrostDevice::disable_perfcnt() const
{
	drm_panfrost_perfcnt_enable perfcnt = {};
	perfcnt.enable = 0; // disable
	auto res = drmIoctl( fd, DRM_IOCTL_PANFROST_PERFCNT_ENABLE, &perfcnt );
	check( res, "Cannot disable perfcnt" );
}

} // namespace pps::gpu
