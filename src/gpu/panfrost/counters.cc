/*
 * Copyright (c) 2019-2020 Collabora, Ltd.
 * Author: Antonio Caggiano <antonio.caggiano@collabora.com>
 *
 * SPDX-License-Identifier: MIT
 */

#include "pps/gpu/panfrost/counters.h"

#include <cassert>
#include <cstring>
#include <pps/algorithm.h>

#include "pps/gpu/panfrost_device.h"


namespace pps::gpu
{


/// @return The block id of a counter given its offset within the counter names
CounterBlock find_block(const int32_t offset)
{
	int32_t block = offset / 64;

	switch (block) {
	case 0:
	case 1:
		break;
	case 2:
		block = 3;
		break;
	case 3:
		block = 2;
		break;
	default:
		assert(false && "Invalid counter block");
		return CounterBlock::UNDEFINED;
	}

	return static_cast<CounterBlock>(block);
}



Counter::Value Counter::default_getter( const Counter& counter, const Device& device )
{
	assert( counter.block != CounterBlock::UNDEFINED && "Block should not be undefined" );
	uint32_t block_index = to_u32( counter.block );
	uint32_t block_offset = counter.offset % PanfrostDevice::counters_per_block;

	int64_t value = device.counters[block_index * PanfrostDevice::counters_per_block + block_offset];

	// Shader cores
	if ( counter.block == CounterBlock::SHADER_CORE ) {
		// Accumulate values from other cores
		for ( size_t core = 1; core < device.cores; ++core ) {
			size_t pos = ( block_index + core ) * PanfrostDevice::counters_per_block + block_offset;
			value += device.counters[pos];
		}
	}

	return value;
}


Counter::Counter( const char* nam, const CounterBlock bloc )
: name { nam }
, block { bloc }
{}


Counter::Counter( const char* nam, const int32_t offse )
: name { nam }
, offset { offse }
, block { find_block( offset ) }
{}


bool Counter::operator==( const Counter& other ) const
{
	return std::strcmp( name, other.name ) == 0;
}


uint32_t Counter::get_id( const Device& device ) const
{
	auto it = FIND( device.available_counters, *this );
	assert( it != std::end( device.available_counters ) && "Counter is not available" );

	return it - std::begin( device.available_counters );
}


bool Counter::is_derived() const
{
	return getter.target<Getter>() != default_getter;
}


} // namespace pps::gpu
