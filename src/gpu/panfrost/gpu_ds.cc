/*
 * Copyright (c) 2019-2020 Collabora, Ltd.
 * Author: Antonio Caggiano <antonio.caggiano@collabora.com>
 * Author: Rohan Garg <rohan.garg@collabora.com>
 *
 * SPDX-License-Identifier: MIT
 */

#include "gpu_ds.h"

#include <thread>
#include <cassert>
#include <cstring>
#include <variant>
#include <pps/algorithm.h>

#include "pps/gpu/panfrost_device.h"

namespace pps::gpu
{

std::string PanfrostDataSource::drm_device_name = "default";


void PanfrostDataSource::enable( const Counter& counter )
{
	enabled_counters.emplace_back( counter );
	PERFETTO_LOG( "%s enabled", counter.name );
}


void PanfrostDataSource::OnSetup( const SetupArgs& args )
{
	// Setup the device
	if ( !device )
	{
		device = std::make_unique<PanfrostDevice>();
		if ( !device )
		{
			PERFETTO_FATAL( "Could not find DRM device %s", drm_device_name.c_str() );
		}
	}

	// Parse perfetto config
	const std::string& config_raw = args.config->gpu_counter_config_raw();
	perfetto::protos::pbzero::GpuCounterConfig::Decoder config( config_raw );

	assert( enabled_counters.empty() && "There should be no enabled counters at this point" );
	if ( config.has_counter_ids() )
	{
		/// Get enabled counters
		PERFETTO_ILOG( "Selecting counters" );
		for ( auto it = config.counter_ids(); it; ++it )
		{
			enable( device->available_counters[it->as_uint32()] );
		}
	}
	else
	{
		// Enable all counters
		for ( Counter& counter : device->available_counters )
		{
			enable( counter );
		}
	}

	// Get sampling period
	if ( config.has_counter_period_ns() )
	{
		sampling_period_ns = config.counter_period_ns();
		PERFETTO_LOG( "Sampling period set to %lu ns", sampling_period_ns );
		if ( sampling_period_ns < min_sampling_period_ns )
		{
			PERFETTO_ELOG( "Sampling period should be greater than 1000000 ns (1 ms)" );
			PERFETTO_ELOG( "Sampling period set to 1 ms to minimize impact intrusiveness of tracing" );
			sampling_period_ns = min_sampling_period_ns;
		}
	}

	PERFETTO_LOG( "Initialization finished" );
}


void PanfrostDataSource::OnStart( const StartArgs& args )
{
	device->open();
	device->enable_perfcnt();

	state = State::Start;
	PERFETTO_LOG( "OnStart finished" );
}


void close_callback( PanfrostDataSource::TraceContext ctx )
{
	auto packet = ctx.NewTracePacket();
	packet->set_timestamp( perfetto::base::GetBootTimeNs().count() );
	auto test = packet->set_for_testing();
	test->set_str( "Last packet" );
	packet->Finalize();
	ctx.Flush();
	PERFETTO_LOG( "Context flushed" );
}


void PanfrostDataSource::OnStop( const StopArgs& args )
{
	auto stop_closure = args.HandleStopAsynchronously();
	Trace( close_callback );
	stop_closure();
	
	state = State::Stop;

	device->disable_perfcnt();
	device->close();

	enabled_counters.clear();

	PERFETTO_LOG( "OnStop finished" );
}


/// @brief Sends GPU counter blocks (or categories)
void add_blocks(
	const std::vector<Counter>& enabled_counters,
	const Device& device,
	perfetto::protos::pbzero::GpuCounterDescriptor& desc )
{
	using namespace perfetto::protos::pbzero;

	// Define blocks descs
	constexpr uint32_t block_count = to_u32( CounterBlock::MAX_VALUE );
	std::array<GpuCounterDescriptor::GpuCounterBlock*, block_count> block_descs;

	// Populate block descs
	for ( uint32_t block_index = 0;
	      block_index < block_count;
	      ++block_index )
	{
		auto block = CounterBlock( block_index );

		// Define block properties
		auto block_desc = desc.add_blocks();
		block_desc->set_name( to_string( block ) );
		block_desc->set_block_id( block_index );

		block_descs[block_index] = block_desc;
	}

	for ( auto& counter : enabled_counters )
	{
		// Associate counters to blocks
		block_descs[to_u32( counter.block )]->add_counter_ids( counter.get_id( device ) );
	}
}


/// @brief Set description of the counter
void set_spec(
	const Counter& counter,
	const Device& device,
	perfetto::protos::pbzero::GpuCounterDescriptor::GpuCounterSpec& spec )
{
	uint32_t id = counter.get_id( device );
	spec.set_counter_id( id );

	std::string num = "000. ";
	std::sprintf( num.data(), "%03u", id );
	num[3] = '.';
	assert( std::strcmp( counter.name, "" ) && "Cannot send description of an empty counter name" );
	spec.set_name( num + counter.name );
}


double ratio( double numerator, double denominator )
{
	return denominator > 0.0 ? numerator / denominator : 0.0;
}


void PanfrostDataSource::set_counter_event(
	const Counter& counter,
	perfetto::protos::pbzero::GpuCounterEvent::GpuCounter& counter_event )
{
	counter_event.set_counter_id( counter.get_id( *device ) );

	auto value = counter.get_value( *device );

	if ( auto long_v = std::get_if<int64_t>( &value ) )
	{
		counter_event.set_int_value( *long_v );
	}
	else if ( auto double_v = std::get_if<double>( &value ) )
	{
		counter_event.set_double_value( *double_v );
	}
}


void PanfrostDataSource::trace( TraceContext& ctx )
{
	using namespace perfetto::protos::pbzero;

	if ( !device->dump_perfcnt() )
	{
		// Do not send any packet in case of error
		PERFETTO_ELOG( "Skipping trace" );
		return;
	}

	auto packet = ctx.NewTracePacket();
	packet->set_timestamp( perfetto::base::GetBootTimeNs().count() );

	auto event = packet->set_gpu_counter_event();
	event->set_gpu_id( 0 );

	if ( auto state = ctx.GetIncrementalState(); state->was_cleared )
	{
		PERFETTO_ILOG( "Sending counter descriptors" );
		auto desc = event->set_counter_descriptor();
		desc->set_min_sampling_period_ns( min_sampling_period_ns );

		add_blocks( enabled_counters, *device, *desc );

		for ( auto counter : enabled_counters )
		{
			auto spec = desc->add_specs();
			set_spec( counter, *device, *spec );
		}

		state->was_cleared = false;
	}

	for ( auto counter : enabled_counters )
	{
		auto counter_event = event->add_counters();
		set_counter_event( counter, *counter_event );
	}
}


void PanfrostDataSource::trace_callback( TraceContext ctx )
{
	auto state = State::Stop;
	std::chrono::nanoseconds sleep_time;

	if ( auto data_source = ctx.GetDataSourceLocked() )
	{
		state = data_source->state;
		sleep_time = std::chrono::nanoseconds( data_source->sampling_period_ns ) - data_source->time_to_trace;
	}

	if ( state == State::Stop )
	{
		// Not started yet, just wait until it starts
		sleep_time = std::chrono::milliseconds( 2 );
		std::this_thread::sleep_for( sleep_time );
		return;
	}

	// Wait sampling period before tracing
	std::this_thread::sleep_for( sleep_time );

	auto time_zero = perfetto::base::GetBootTimeNs();
	if ( auto data_source = ctx.GetDataSourceLocked() )
	{
		// Check data source is still running
		if ( data_source->state == pps::State::Start )
		{
			data_source->trace( ctx );
			data_source->time_to_trace = perfetto::base::GetBootTimeNs() - time_zero;
		}
	}
	else
	{
		// Data source has been destroyed, just finish
		PERFETTO_LOG( "Data source has been destroyed" );
	}
}


} // namespace pps::gpu
