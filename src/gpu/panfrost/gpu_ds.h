/*
 * Copyright (c) 2019-2020 Collabora, Ltd.
 * Author: Antonio Caggiano <antonio.caggiano@collabora.com>
 *
 * SPDX-License-Identifier: MIT
 */

#pragma once

#include <pps/pds.h>
#include <pps/gpu/device.h>

namespace pps::gpu
{
struct PanfrostIncrementalState
{
	bool was_cleared = true;
};

struct PanfrostDataSourceTraits : public perfetto::DefaultDataSourceTraits
{
	using IncrementalStateType = PanfrostIncrementalState;
};

using counter_t = uint32_t;

class PanfrostDataSource
: public perfetto::DataSource<PanfrostDataSource, PanfrostDataSourceTraits>
{
  public:
	static constexpr const char* name = "gpu.metrics";

	/// Minimum sampling period is 1 millisecond
	static constexpr uint64_t min_sampling_period_ns = 1000000;

	/// Name of the DRM device which this data source will try to open
	static std::string drm_device_name;

	void OnSetup( const SetupArgs& args ) override;
	void OnStart( const StartArgs& args ) override;
	void OnStop( const StopArgs& args ) override;

	/// @brief Perfetto trace callback
	static void trace_callback( TraceContext ctx );

	void trace( TraceContext& ctx );

  private:
	State state = State::Stop;

	uint64_t sampling_period_ns = min_sampling_period_ns;

	/// @brief Enables a specific counter
	void enable( const Counter& counter );

	/// @brief Set values of a specific counter to be sent through a trace packet
	void set_counter_event( const Counter& counter,
		perfetto::protos::pbzero::GpuCounterEvent::GpuCounter& counter_event );

	std::unique_ptr<Device> device;

	/// List of counters to sample
	std::vector<Counter> enabled_counters;

	perfetto::base::TimeNanos time_to_trace;
};


}  // namespace pps::gpu
