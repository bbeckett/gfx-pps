/*
 * Copyright (c) 2019-2020 Collabora, Ltd.
 * Author: Antonio Caggiano <antonio.caggiano@collabora.com>
 * Author: Rohan Garg <rohan.garg@collabora.com>
 *
 * SPDX-License-Identifier: MIT
 */

#include "pps/gpu/device.h"

#include <fcntl.h>
#include <xf86drm.h>
#include <pps/perfetto/perfetto.h>
#include <pps/algorithm.h>

#define MAX_DRM_DEVICES 64

namespace pps::gpu
{


/// @return Whether a DRM device has a specific version name
bool has_version_name( const int fd, const char* version_name )
{
	drmVersionPtr version = drmGetVersion(fd);
	bool ret = false;

	if ( version && strcmp( version->name, version_name ) == 0 )
	{
		ret = true;
	}
	
	if ( version )
	{
		drmFreeVersion(version);
	}

	return ret;
}


/// @brief Looks for a DRM device with a specific version name
/// @param version_name DRM version name
/// @return A file descriptor of the graphics device, or -1 if not found
int open_device( const char* version_name )
{
	drmDevicePtr devices[MAX_DRM_DEVICES], device;
	drmVersionPtr version;
	int i, num_devices, fd = -1;

	num_devices = drmGetDevices2(0, devices, MAX_DRM_DEVICES);
	if (num_devices <= 0)
	{
		PERFETTO_LOG("Cound not get any DRM devices.");
		return fd;
	}

	for ( i = 0; i < num_devices; i++ )
	{
		device = devices[i];
		if ( (device->available_nodes & (1 << DRM_NODE_RENDER) ) &&
			(device->bustype == DRM_BUS_PLATFORM))
		{
			fd = open( device->nodes[DRM_NODE_RENDER], O_RDONLY );
			if ( fd < 0 )
				continue;

			if ( has_version_name( fd, version_name ) )
				break;

			close(fd);
			fd = -1;
		}
	}

	drmFreeDevices(devices, num_devices);
	return fd;
}


Device::Device( const char* version_name )
: fd { open_device( version_name ) }
{
	if ( !is_open() )
	{
		return;
	}

	name = version_name;
}


Device::Device( Device&& o )
: fd { o.fd }
, name { std::move( o.name ) }
, cores { o.cores }
, available_counters { std::move( o.available_counters ) }
, counters { std::move( o.counters ) }
, tile_size { o.tile_size }
{
	o.fd = -1;
}


Device& Device::operator=( Device&& o )
{
	std::swap( fd , o.fd );
	std::swap( name , o.name );
	std::swap( cores, o.cores );
	std::swap( available_counters , o.available_counters );
	std::swap( counters , o.counters );
	std::swap( tile_size, o.tile_size );
	return *this;
}


Device::~Device()
{
	if ( is_open() )
	{
		close();
	}
}

void Device::open()
{
	if ( !is_open() )
	{
		fd = open_device( name.c_str() );
	}
}


void Device::close()
{
	if ( is_open() )
	{
		::close( fd );
		fd = -1;
	}
}


} // namespace pps::gpu
