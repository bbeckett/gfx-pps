Graphics Perfetto producers
---------------------------

This project contains a collection of graphics-related [Perfetto producers](https://perfetto.dev/#/architecture.md). A producer is a client process for the Perfetto tracing service. Currently under development are the following producers:

- Panfrost performance counters
- Weston timeline debug stream

## Build

This section guides you through the building process assuming you are compiling on Ubuntu.

1. Install dependencies:

```sh
apt install build-essential meson cmake libwayland-dev rapidjson-dev libdrm-dev libprotobuf-dev libgtest-dev libdocopt-dev
```

2. Generate the project:

```sh
meson build
```

3. Compile and run tests (it may take a while):

```sh
ninja -C build test
```

### Cross-compile

In order to capture a trace on a remote arm64 target, Perfetto and the gfx-pps producers need to be cross-compiled for Linux aarch64.

#### Cross-compile perfetto

It is not possible to compile Perfetto on Linux aarch64 as you need to run `tools/gn`, which is not compiled for that architecture.

To cross-compile on amd64 using clang, follow these steps:

1. Change directory to `perfetto`

```sh
cd <path>/perfetto
```

2. Generate a build directory

```sh
tools/gn args out/arm64
```

Typing in the following GN args

```js
// args.gn
target_os = "linux"
target_cpu = "arm64"
is_debug = false
target_sysroot = "/usr/aarch64-linux-gnu"
target_gcc_toolchain = "/usr"
```

3. Compile perfetto and the tracing service

```sh
tools/ninja -C out/arm64 perfetto traced traced_probes
```

#### Cross-compile gfx-pps producers

To cross-compile with meson, you need to take these steps:

1. Prepare a chroot for cross-building

```sh
sudo debootstrap --variant=buildd focal /srv/target/focal
sudo chroot /srv/target/focal
mount -t proc proc /proc
```

2. Add arm64 to `/etc/apt/souces.list`

```
deb [arch=amd64] http://archive.ubuntu.com/ubuntu focal main
deb [arch=arm64] http://ports.ubuntu.com/ubuntu-ports focal main
```

3. Enable multiarch by adding arm64

```sh
dpkg --add-architecture arm64
```

4. Install the arm64 toolchain and other required packages:

```sh
apt update
apt install build-essential libc6-arm64-cross gcc-aarch64-linux-gnu g++-aarch64-linux-gnu meson cmake libdrm-dev:arm64 libwayland-dev:arm64 libprotobuf-dev:arm64 rapidjson-dev:arm64 libgtest-dev:arm64 libdocopt-dev:arm64
```

5. Create a [cross file](https://mesonbuild.com/Cross-compilation.html) for meson like the following snippet: [aarch64-linux](https://gitlab.freedesktop.org/Fahien/gfx-pps/snippet/1086)

6. Generate the project using that cross file:

```sh
meson build/arm64 --cross-file aarch64-linux
```

7. Compile and run tests

```sh
ninja -C build/arm64 test
```

## Run

To capture a trace with perfetto you need to take the following steps:

1. Create a [trace config](https://perfetto.dev/#/trace-config.md), which is a json formatted text file with extension `.cfg`, or use one of those under the `script` directory.

2. Copy the config file to `perfetto/test/configs`. Under this directory you can also find more example of trace configs.

3. Change directory to `perfetto` and run a [convenience script](https://perfetto.dev/#/running.md) to start the tracing service:

```sh
cd <path>/perfetto
CONFIG=test.cfg OUT=out/build ./tools/tmux
```

4. Start other producers you may need, like `producer-gpu` or `producer-weston`.

5. Start perfetto under the tmux session initiated in step 3.

6. Once tracing has finished, you can detach from tmux with <kbd>ctrl</kbd>+<kbd>b</kbd>, <kbd>d</kbd>, and the convenience script should automatically copy the trace files into `$HOME/Downloads`.

7. Go to [ui.perfetto.dev](https://ui.perfetto.dev) and upload `$HOME/Downloads/trace.protobuf` by clicking on *Open trace file*.

### GPU producer

The GPU producer contains at the current state a data-source able to query GPU performance counters using the Panfrost driver. The data-source uses unstable ioctls that behave correctly on kernel version [5.4.23+](https://lwn.net/Articles/813601/) and [5.5.7+](https://lwn.net/Articles/813600/). To run the producer, follow these two simple steps:

1. Enable Panfrost unstable ioctls via kernel parameter:

```sh
modprobe panfrost unstable_ioctls=1
```

2. Run the producer:

```sh
./build/producer-gpu
```

### Weston producer

The weston producer needs the debug protocol to be present in weston. If the debug protocol is not present, update weston or install the latest development version from git.

1. Before running `producer-weston`, make sure weston is started with the `--debug` argument:

```sh
weston --debug
```

2. Execute the producer:

```sh
./build/producer-weston
```

## Troubleshooting

If the convenience script `tools/tmux` keeps copying artifacts to your `SSH_TARGET` without starting the tmux session, make sure you have `tmux` installed in your system.

```sh
apt install tmux
```
