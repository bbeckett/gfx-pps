/*
 * Copyright (c) 2020 Collabora, Ltd.
 * Author: Antonio Caggiano <antonio.caggiano@collabora.com>
 *
 * SPDX-License-Identifier: MIT
 */

#include <cstring>
#include <gtest/gtest.h>
#include <pps/gpu/panfrost_device.h>
#include <pps/gpu/panfrost/hwc_names.h>

#include <pps/algorithm.h>


namespace pps::gpu
{


#define GET_COUNTER_NAMES( b ) std::vector<const char*>( b, b + sizeof( b ) / sizeof( b[0] ) )


PanfrostDevice create_mali_t604()
{
	auto device = PanfrostDevice();
	device.cores = 1;
	device.counters.resize( 64 * ( device.cores + 3 ) );
	auto counter_names = GET_COUNTER_NAMES( mali_userspace::hardware_counters_mali_t60x );
	return device;
}


PanfrostDevice create_mali_t880()
{
	auto device = PanfrostDevice();
	device.cores = 1;
	device.counters.resize( 64 * ( device.cores + 3 ) );
	auto counter_names = GET_COUNTER_NAMES( mali_userspace::hardware_counters_mali_t88x );
	return device;
}


PanfrostDevice create_mali_t860_mp4()
{
	auto device = PanfrostDevice();
	device.cores = 4;
	device.counters.resize( 64 * ( device.cores + 3 ) );
	auto counter_names = GET_COUNTER_NAMES( mali_userspace::hardware_counters_mali_t86x );
	device.available_counters = create_available_counters( counter_names );
	return device;
}


void test_counters( const PanfrostDevice& device )
{
	for ( uint32_t i = 1; i < device.available_counters.size(); ++i )
	{
		const Counter& counter = device.available_counters[i];
		if ( !counter.is_derived() )
			EXPECT_NE( 0, counter.offset );
	}
}


TEST( Panfrost, Counter )
{
	test_counters( create_mali_t604() );
	test_counters( create_mali_t860_mp4() );
	test_counters( create_mali_t880() );
}


TEST( Panfrost, CountersCount )
{
	EXPECT_EQ( query_counters_count( 4 ), 1792 / sizeof( uint32_t ) );
}


TEST( Panfrost, PerfCnt )
{
	auto dump_path = "test/data/gpu/panfrost/perfcnt.dump";
	auto dump_file = std::fopen( dump_path, "rb" );
	EXPECT_TRUE( dump_file != nullptr );

	std::fseek( dump_file, 0, SEEK_END );
	size_t dump_size = std::ftell( dump_file );
	std::fseek( dump_file, 0, SEEK_SET );
	EXPECT_EQ( dump_size, 1792 );

	PanfrostDevice device { create_mali_t860_mp4() };

	size_t counter_count = device.counters.size();

	std::fread( device.counters.data(), sizeof( uint32_t ), counter_count, dump_file );

	auto it = FIND_IF( device.available_counters, []( Counter& c ) { return std::strstr( c.name, "FRAG_ACTIVE" ); } );
	EXPECT_NE( it, std::end( device.available_counters ) );
	Counter frag_active = *it;

	EXPECT_EQ( frag_active.block, CounterBlock::SHADER_CORE );

	uint32_t block_index = to_u32( frag_active.block );
	size_t block_offset = frag_active.offset % PanfrostDevice::counters_per_block;

	// Check shader core 0
	size_t core0_offset = block_index * PanfrostDevice::counters_per_block + block_offset;
	EXPECT_LT( core0_offset, counter_count );
	uint32_t core0_value = device.counters[core0_offset];
	EXPECT_EQ( core0_value, 91435465 );

	// Check shader core 1
	size_t core1_offset = (block_index + 1) * PanfrostDevice::counters_per_block + block_offset;
	EXPECT_LT( core1_offset, counter_count );
	uint32_t core1_value = device.counters[core1_offset];
	EXPECT_EQ( core1_value, 91439247 );

	// Check shader core 2
	size_t core2_offset = (block_index + 2) * PanfrostDevice::counters_per_block + block_offset;
	EXPECT_LT( core2_offset, counter_count );
	uint32_t core2_value = device.counters[core2_offset];
	EXPECT_EQ( core2_value, 91417873 );

	// Check shader core 3
	size_t core3_offset = (block_index + 3) * PanfrostDevice::counters_per_block + block_offset;
	EXPECT_LT( core3_offset, counter_count );
	uint32_t core3_value = device.counters[core3_offset];
	EXPECT_EQ( core3_value, 91409969 );

	EXPECT_EQ( std::get<int64_t>( frag_active.get_value( device ) ),
		core0_value + core1_value + core2_value + core3_value );
}


} // namespace pps::gpu


int main( int argc, char** argv )
{
	testing::InitGoogleTest( &argc, argv );
	return RUN_ALL_TESTS();
}
