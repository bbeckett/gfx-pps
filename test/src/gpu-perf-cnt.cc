#include <pps/gpu/panfrost_device.h>

#include <cstdlib>
#include <cstring>
#include <thread>
#include <docopt/docopt.h>


static const char* USAGE =
R"(gpu-perf-cnt

  Usage:
	gpu-perf-cnt dump [--sec=<n>]
	gpu-perf-cnt ls
	gpu-perf-cnt (-h | --help)
	gpu-perf-cnt --version

  Options:
	-h --help	Show this screen.
	--version	Show version.
	--sec=<n>	Seconds to wait before dumping performance counters [default: 0].
)";


// Tool running mode
enum class Mode
{
	// Show help message
	Help,
	// Show list of available counters
	List,
	// Dump performance counters
	Dump,
};


int main( int argc, const char** argv )
{
	using namespace pps;

	Mode mode = Mode::Help;
	auto secs = std::chrono::seconds( 0 );

	auto args = docopt::docopt( USAGE, { std::next( argv ), std::next( argv, argc ) }, true, "gpu-perf-cnt 0.1" );

	if ( args["dump"].asBool() )
		mode = Mode::Dump;

	if ( args["--sec"] )
		secs = std::chrono::seconds( args["--sec"].asLong() );

	if ( args["ls"].asBool() )
		mode = Mode::List;

	// Docopt shows the help message for us
	if ( mode == Mode::Help )
		return EXIT_SUCCESS;

	auto device = gpu::PanfrostDevice();
	device.open();
	if ( !device.is_open() ) {
		fprintf( stderr, "Failed to open device\n" );
		return EXIT_FAILURE;
	}

	switch ( mode ) {
	case Mode::Dump:
		device.enable_perfcnt();
		std::this_thread::sleep_for( std::chrono::seconds( secs ) );
		device.dump_perfcnt();
		device.disable_perfcnt();

		std::fwrite(
			device.get_counters().data(),
			sizeof( uint32_t ),
			device.get_counters().size(),
			stdout );

		break;
	case Mode::List:
		for ( uint32_t i = 0; i < device.available_counters.size(); ++i ) {
			auto& counter = device.available_counters[i];
			printf( "%u - %s\n", counter.get_id( device ), counter.name );
		}

		break;
	} // switch

	return EXIT_SUCCESS;
}
